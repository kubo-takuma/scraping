# Selenium ON Docker

## 概要

Dockerで

コンテナ関連図

~~~
 python
   |  link
selenium-hub
   | link
 chrome
~~~

pythonコンテナからhubコンテナのdriverに対し、命令を行い、
chromeコンテナで、接続しに行きます。

そのため、pythonにはvim・seleniumをインストール
その後、selenium-hubコンテナに接続するため、proxyをunsetします


##  手順

- DockerImageの作成
- docker-composeの実行
- pythonコンテナに入り、命令の実行（VNCで確認）



$ mkdir selenium
$ cd selenium
$ vim Dockerfile

~~~
FROM python:3

# vimとseleniumをインストール
RUN  export http_proxy=http://proxy:3128 && \
     export https_proxy=http://proxy:3128 && \
     set -x && \
     apt-get update && \
     apt-get install -y vim && \
     pip --proxy http://proxy:3128 install selenium && \
     unset http_proxy && \
     unset https_proxy
~~~

$ docker build -t python3 .





$ vim docker-compose.yml
~~~
selenium-hub:
  image: selenium/hub
  container_name: 'selenium-hub'
  ports:
    - 4444:4444

chrome:
  image: selenium/node-chrome-debug
  container_name: 'chrome'
  links:
    - selenium-hub:hub
  ports:
    - 5900:5900
  volumes:
    - /dev/shm:/dev/shm
  

#firefox:
#  image: selenium/node-firefox-debug
#  container_name: 'selenium-firefox'
#  ports:
#   - 5910:5900
#  links:
#    - selenium-hub:hub

python:
  image: python3:latest
  container_name: 'python'
  links:
    - selenium-hub:hub
  command: 'tail -f /dev/null'
  working_dir: '/root/'
  volumes:
    - ./script/:/root/script/
~~~

$ docker-compose up -d

$ docker exec -it python /bin/bash

docker$ python 
~~~

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver import Proxy
proxy = Proxy()
proxy.ftp_proxy = proxy.ssl_proxy = proxy.http_proxy = 'http://proxy:3128'

driver = webdriver.Remote(
            command_executor='http://selenium-hub:4444/wd/hub',
            proxy=proxy,
            desired_capabilities=DesiredCapabilities.CHROME)
driver.get("https://google.com")
driver.close()

~~~



### pythonで動くかテスト

~~~

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver import Proxy
proxy = Proxy()
proxy.ftp_proxy = proxy.ssl_proxy = proxy.http_proxy = 'http://proxy:3128'

driver = webdriver.Remote(
            command_executor='http://selenium-hub:4444/wd/hub',
            proxy=proxy,
            desired_capabilities=DesiredCapabilities.CHROME)
driver.get("https://www.cman.jp/network/support/go_access.cgi")
driver.close()




~~~
