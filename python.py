mport os
import requests 
import re
import sys
from bs4 import BeautifulSoup
from urllib.parse import urlparse
# selenium関連
from selenium import webdriver
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver import Proxy
proxy = Proxy()
proxy.ftp_proxy = proxy.ssl_proxy = proxy.http_proxy = 'http://proxy:3128'
driver = webdriver.Remote(
            command_executor='http://selenium-hub:4444/wd/hub',
            proxy=proxy,
            desired_capabilities=DesiredCapabilities.CHROME)


# 電話番号をキレイにする
pattern = r'[\(]{0,1}[0-9]{2,4}[\)\-\(]{0,1}[0-9]{2,4}[\)\-]{0,1}[0-9]{3,4}'


# inputURLをどの媒体か判断して各媒体ごとの処理に飛ばす
def judgeDomain(inputURL):
    URL = inputURL
    d = urlparse(inputURL).netloc
    if d == "townwork.net":
        townwork(URL)
    elif d == "baito.mynavi.jp":
        print("ぬたういy")
    else:
        print("まだ対応してません。待っててね。")


# タウンワーク
def townwork(URL):
    soup = GetHTML(URL)
    decide = len(soup.find_all("h2"))
    if decide ==6:
        tags = soup.select("#jsi-content-wrapper > div.contents-wrap > div.contents-main-wrap.jsc-job-trigger > div.job-detail-box-wrap > div:nth-child(9)")
    elif decide  ==7:
        tags = soup.select("#jsi-content-wrapper > div.contents-wrap > div.contents-main-wrap.jsc-job-trigger > div.job-detail-box-wrap > div:nth-child(10)")
        soup.find(class_="company-ttl-ico ico-arrow")
    # 企業情報
    data = tags[0].select("dd> p")
    links = [t.text for t in data]
    company = links[0]
    address = links[2]
    # URLが無い原稿だと空欄を入れる
    company_url = tags[0].select("dd> a")
    try:
        HomePage = company_url[0].text
    except IndexError:
        HomePage = ""
    tel_data = soup.select("#jsi-content-wrapper > div.contents-wrap > div.contents-main-wrap.jsc-job-trigger > div.job-detail-box-wrap > div:nth-child(9) > div > dl:nth-child(4) > dd > div > div > p.detail-tel-ttl > span")
    try:
        TEL = re.findall(pattern, tel_data[0].text)[0]
    except IndexError:
        TEL = ""
    # seleniumを起動させて書く
    write(company,address,TEL,HomePage,URL)
    


# HTMLを取得する
def GetHTML(URL):
    os.environ["HTTP_PROXY"] = "http://proxy:3128"
    os.environ["HTTPS_PROXY"] = "http://proxy:3128"
    soup = BeautifulSoup(requests.get(URL).text, "html.parser")
    return soup


def write(company,address,TEL,HomePage,URL):
    # ログインする
    driver.get("https://my.saaske.com")
    iframe = driver.find_element_by_name('admin')
    driver.switch_to.frame(iframe)

    # loginしてLEADに移動
    driver.find_element_by_name('ac_id').send_keys('kubo.takuma@mynavi.jp')
    driver.find_element_by_name('ac_pass').send_keys('Awsh98mn^^')
    driver.find_element_by_name('submit').click()
    driver.find_element_by_xpath('//*[@id="mCSB_2_container"]/div/h2[1]/a').click()
    
    
    # 新規登録への移動
    # 下にスクロール出来ないので、全顧客一覧から登録する。
    driver.find_element_by_link_text(u"全顧客一覧").click()
    driver.find_element_by_xpath('//*[@id="main"]/table[1]/caption/div/form/input[4]').click()
    
    
    # 新規登録画面
    driver.execute_script("window.scrollTo(0, 2000);")
    
    # 入力
    driver.find_element_by_id('dt_company').send_keys(company)
    driver.find_element_by_id('dt_hp').send_keys(HomePage)
    driver.find_element_by_id('dt_add1').send_keys(address)
    driver.find_element_by_id('dt_tel').send_keys(TEL)
    
    # 登録
    driver.execute_script("window.scrollTo(0, 5000);")
    driver.find_element_by_xpath("//*[@id='main']/form/div/input[1]").click()
    Alert(driver).accept()
    
    driver.quit()

