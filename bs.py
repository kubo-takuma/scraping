
p --proxy=http://proxy:3128 install --upgrade pip
pip --proxy=http://proxy:3128 install beautifulsoup4 requests

import os
import requests 
import re
import sys
from bs4 import BeautifulSoup
from urllib.parse import urlparse


# 電話番号をキレイにする
pattern = r'[\(]{0,1}[0-9]{2,4}[\)\-\(]{0,1}[0-9]{2,4}[\)\-]{0,1}[0-9]{3,4}'


# inputURLをどの媒体か判断して各媒体ごとの処理に飛ばす
def judgeDomain(inputURL):
    URL = inputURL
    d = urlparse(inputURL).netloc
    if d == "townwork.net":
        townwork(URL)
    elif d == "baito.mynavi.jp":
        print("ぬたういy")
    else:
        print("まだ対応してません。待っててね。")


# タウンワーク
def townwork(URL):
    soup = GetHTML(URL)
    decide = len(soup.find_all("h2"))
    if decide ==6:
        tags = soup.select("#jsi-content-wrapper > div.contents-wrap > div.contents-main-wrap.jsc-job-trigger > div.job-detail-box-wrap > div:nth-child(9)")
    elif decide  ==7:
        tags = soup.select("#jsi-content-wrapper > div.contents-wrap > div.contents-main-wrap.jsc-job-trigger > div.job-detail-box-wrap > div:nth-child(10)")
        soup.find(class_="company-ttl-ico ico-arrow")
    # 企業情報
    data = tags[0].select("dd> p")
    links = [t.text for t in data]
    company = links[0]
    address = links[2]
    
    # URLが無い原稿だと空欄を入れる
    company_url = tags[0].select("dd> a")
    try:
        URL = company_url[0].text
    except IndexError:
        URL = ""
    
    
    # tel
    tel_data = soup.select("#jsi-content-wrapper > div.contents-wrap > div.contents-main-wrap.jsc-job-trigger > div.job-detail-box-wrap > div:nth-child(9) > div > dl:nth-child(4) > dd > div > div > p.detail-tel-ttl > span")
    try:
        TEL = re.findall(pattern, tel_data[0].text)[0]
    except IndexError:
        TEL = ""
    
    #return(company,address,TEL,URL)
    # print(company,address,TEL,URL)


# HTMLを取得する
def GetHTML(URL):
    print("GetHTML")
    os.environ["HTTP_PROXY"] = "http://proxy:3128"
    os.environ["HTTPS_PROXY"] = "http://proxy:3128"
    soup = BeautifulSoup(requests.get(URL).text, "html.parser")
    return soup



